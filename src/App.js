import './App.css';
import Number from "./Components/Number/Number";
import {Component} from "react";

class App extends Component {
    state = {
        numbersArray: []
    };
    changeNumber = () => {
        console.log(this.state.numbersArray);
        const numbersArrayCopy = [];

        while (true) {
            const randomNumber = this.getRndInteger(5, 36);
            if (!numbersArrayCopy.includes(randomNumber)) {
                numbersArrayCopy.push(randomNumber);
            }
            if (numbersArrayCopy.length === 5) {
                break;
            }
        }
        this.setState({
            numbersArray: numbersArrayCopy.sort((a, b) => a > b ? 1 : -1)
        });
    };
    getRndInteger = (min, max) => {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    render() {
        return (
            <div className={"App"}>
                <div>
                    <button className={"button"} onClick={this.changeNumber}>
                        New numbers
                    </button>
                </div>
                <Number>{this.state.numbersArray[0]}</Number>
                <Number>{this.state.numbersArray[1]}</Number>
                <Number>{this.state.numbersArray[2]}</Number>
                <Number>{this.state.numbersArray[3]}</Number>
                <Number>{this.state.numbersArray[4]}</Number>
            </div>
        );
    }
}

export default App;
